# coding: utf-8

require "nokogiri"
require "open3"

module Books
  DATA = [
    [ "Genesis",            "Gen",    "Ge"   ],
    [ "Exodus",             "Exod",   "Ex"   ],
    [ "Leviticus",          "Lev",    "Le"   ],
    [ "Numbers",            "Num",    "Nu"   ],
    [ "Deuteronomy",        "Deut",   "De"   ],
    [ "Joshua",             "Josh",   "Jos"  ],
    [ "Judges",             "Judg",   "Jg"   ],
    [ "Ruth",               "Ruth",   "Ru"   ],
    [ "I Samuel",           "1Sam",   "1Sa"  ],
    [ "II Samuel",          "2Sam",   "2Sa"  ],
    [ "I Kings",            "1Kgs",   "1Ki"  ],
    [ "II Kings",           "2Kgs",   "2Ki"  ],
    [ "I Chronicles",       "1Chr",   "1Ch"  ],
    [ "II Chronicles",      "2Chr",   "2Ch"  ],
    [ "Ezra",               "Ezra",   "Ezr"  ],
    [ "Nehemiah",           "Neh",    "Ne"   ],
    [ "Esther",             "Esth",   "Es"   ],
    [ "Job",                "Job",    "Job"  ],
    [ "Psalms",             "Ps",     "Ps"   ],
    [ "Proverbs",           "Prov",   "Pr"   ],
    [ "Ecclesiastes",       "Eccl",   "Ec"   ],
    [ "Song of Solomon",    "Song",   "Song" ],
    [ "Isaiah",             "Isa",    "Isa"  ],
    [ "Jeremiah",           "Jer",    "Jer"  ],
    [ "Lamentations",       "Lam",    "La"   ],
    [ "Ezekiel",            "Ezek",   "Eze"  ],
    [ "Daniel",             "Dan",    "Da"   ],
    [ "Hosea",              "Hos",    "Ho"   ],
    [ "Joel",               "Joel",   "Joe"  ],
    [ "Amos",               "Amos",   "Am"   ],
    [ "Obadiah",            "Obad",   "Ob"   ],
    [ "Jonah",              "Jonah",  "Jon"  ],
    [ "Micah",              "Mic",    "Mic"  ],
    [ "Nahum",              "Nah",    "Na"   ],
    [ "Habakkuk",           "Hab",    "Hab"  ],
    [ "Zephaniah",          "Zeph",   "Zep"  ],
    [ "Haggai",             "Hag",    "Hag"  ],
    [ "Zechariah",          "Zech",   "Zec"  ],
    [ "Malachi",            "Mal",    "Mal"  ],
    [ "Matthew",            "Matt",   "Mt"   ],
    [ "Mark",               "Mark",   "Mr"   ],
    [ "Luke",               "Luke",   "Lu"   ],
    [ "John",               "John",   "Joh"  ],
    [ "Acts",               "Acts",   "Ac"   ],
    [ "Romans",             "Rom",    "Ro"   ],
    [ "I Corinthians",      "1Cor",   "1Co"  ],
    [ "II Corinthians",     "2Cor",   "2Co"  ],
    [ "Galatians",          "Gal",    "Ga"   ],
    [ "Ephesians",          "Eph",    "Eph"  ],
    [ "Philippians",        "Phil",   "Php"  ],
    [ "Colossians",         "Col",    "Col"  ],
    [ "I Thessalonians",    "1Thess", "1Th"  ],
    [ "II Thessalonians",   "2Thess", "2Th"  ],
    [ "I Timothy",          "1Tim",   "1Ti"  ],
    [ "II Timothy",         "2Tim",   "2Ti"  ],
    [ "Titus",              "Titus",  "Tit"  ],
    [ "Philemon",           "Phlm",   "Phm"  ],
    [ "Hebrews",            "Heb",    "Heb"  ],
    [ "James",              "Jas",    "Jas"  ],
    [ "I Peter",            "1Pet",   "1Pe"  ],
    [ "II Peter",           "2Pet",   "2Pe"  ],
    [ "I John",             "1John",  "1Jo"  ],
    [ "II John",            "2John",  "2Jo"  ],
    [ "III John",           "3John",  "3Jo"  ],
    [ "Jude",               "Jude",   "Jude" ],
    [ "Revelation of John", "Rev",    "Re"   ]
  ]

  MAP = { sword: 0, osis: 1, av1611com: 2 }

  def self.name_list(type)
    DATA.map { |x| x[MAP[type]] }
  end

  def self.name(i, type=:sword)
    name_list(type)[i]
  end

  def self.index(book, type=:sword)
    name_list(type).index(book)
  end

  def self.number(book, type=:sword)
    index(book, type) + 1
  end

  def self.translate(book, from, to)
    i = index(book, from)
    raise("FATAL: no book #{book} in #{from}") unless i
    DATA[i][MAP[to]]
  end
end

STATS = {
  ## [1] https://www.biblebelievers.com/believers-org/kjv-stats.html
  ## [2] Ruckman Reference Bible ISBN 1-58026-910-9
  ## When only one source is listed below, then the word count differs wrt the other source;
  ## the chapter and verse count is always supported by both sources.
  ## For Romans to Hebrews, the word count given here is including the colophon,
  ## while the cited sources do not count the words in the colophon.
  "KJV" =>
  {
    "Genesis"            => [ 50, 1533, 38262], # [1,2]
    "Exodus"             => [ 40, 1213, 32685], # [1]
    "Leviticus"          => [ 27,  859, 24541], # [1,2]
    "Numbers"            => [ 36, 1288, 32896], # [1,2]
    "Deuteronomy"        => [ 34,  959, 28352], # [1,2]
    "Joshua"             => [ 24,  658, 18854], # [1,2]
    "Judges"             => [ 21,  618, 18966], # [1,2]
    "Ruth"               => [  4,   85,  2574], # [1,2]
    "I Samuel"           => [ 31,  810, 25048], # [1,2]
    "II Samuel"          => [ 24,  695, 20600], # [1,2]
    "I Kings"            => [ 22,  816, 24513], # [1,2]
    "II Kings"           => [ 25,  719, 23517], # [1,2]
    "I Chronicles"       => [ 29,  942, 20365], # [1,2]
    "II Chronicles"      => [ 36,  822, 26069], # [1,2]
    "Ezra"               => [ 10,  280,  7440], # [1,2]
    "Nehemiah"           => [ 13,  406, 10480], # [1,2]
    "Esther"             => [ 10,  167,  5633], # [1,2]
    "Job"                => [ 42, 1070, 18098], # [1,2]
    "Psalms"             => [150, 2461, 42704-22], # [1] (22 to compensate for the Hebrew alphabet)
    "Proverbs"           => [ 31,  915, 15038], # [1]
    "Ecclesiastes"       => [ 12,  222,  5579], # [1]
    "Song of Solomon"    => [  8,  117,  2658], # [1]
    "Isaiah"             => [ 66, 1292, 37036], # [1]
    "Jeremiah"           => [ 52, 1364, 42654], # [1,2]
    "Lamentations"       => [  5,  154,  3411], # [1,2]
    "Ezekiel"            => [ 48, 1273, 39401], # [1,2]
    "Daniel"             => [ 12,  357, 11602], # [1,2]
    "Hosea"              => [ 14,  197,  5174], # [1,2]
    "Joel"               => [  3,   73,  2033], # [1,2]
    "Amos"               => [  9,  146,  4216], # [1,2]
    "Obadiah"            => [  1,   21,   669], # [1,2]
    "Jonah"              => [  4,   48,  1320], # [1]
    "Micah"              => [  7,  105,  3152], # [1,2]
    "Nahum"              => [  3,   47,  1284], # [1,2]
    "Habakkuk"           => [  3,   56,  1475], # [1,2]
    "Zephaniah"          => [  3,   53,  1616], # [1,2]
    "Haggai"             => [  2,   38,  1130], # [1,2]
    "Zechariah"          => [ 14,  211,  6443], # [1,2]
    "Malachi"            => [  4,   55,  1781], # [1,2]
    "Matthew"            => [ 28, 1071, 23684], # [2]
    "Mark"               => [ 16,  678, 15166], # [2]
    "Luke"               => [ 24, 1151, 25939], # [2]
    "John"               => [ 21,  879, 19094], # [2]
    "Acts"               => [ 28, 1007, 24245], # [2]
    "Romans"             => [ 16,  433,  9422+16], # [1,2]
    "I Corinthians"      => [ 16,  437,  9462+18], # [1]
    "II Corinthians"     => [ 13,  257,  6065+18], # [2]
    "Galatians"          => [  6,  149,  3084+ 6], # [1,2]
    "Ephesians"          => [  6,  155,  3022+ 8], # [1,2]
    "Philippians"        => [  4,  104,  2183+10], # [1,2]
    "Colossians"         => [  4,   95,  1979+10], # [1,2]
    "I Thessalonians"    => [  5,   89,  1837+10], # [1,2]
    "II Thessalonians"   => [  3,   47,  1022+10], # [1,2]
    "I Timothy"          => [  6,  113,  2244+16], # [1,2]
    "II Timothy"         => [  4,   83,  1666+28], # [1,2]
    "Titus"              => [  3,   46,   896+19], # [1,2]
    "Philemon"           => [  1,   25,   430+ 9], # [1,2]
    "Hebrews"            => [ 13,  303,  6897+ 8], # [1]
    "James"              => [  5,  108,  2304], # [1,2]
    "I Peter"            => [  5,  105,  2476], # [1,2]
    "II Peter"           => [  3,   61,  1553], # [1,2]
    "I John"             => [  5,  105,  2517], # [1,2]
    "II John"            => [  1,   13,   298], # [1,2]
    "III John"           => [  1,   14,   294], # [1,2]
    "Jude"               => [  1,   25,   608], # [1,2]
    "Revelation of John" => [ 22,  404, 11995]  # [2]
  }
}

STATS["KJVPCE"] = STATS["KJV"].merge(
  {
    "Exodus"             => [ 40, 1213, 32686], # PCE adds the word "and" in Exodus 23:23
    "Psalms"             => [150, 2461, 42704]  # PCE has the Hebrew alphabet as part of the verses
  })

STATS["KJVPCEx"] = STATS["KJV"].merge(
  {
    "Exodus"             => [ 40, 1213, 32686], # PCE adds the word "and" in Exodus 23:23
  })


def count_words(str)
  return 0 if str.nil?
  str.split.reject { |w| /\A\p{P}*\z/.match(w) }.size
end

def replace_word(str, word_1, word_2, plural_s: false)
  plural_s_ = plural_s ? "(s?)" : ""
  plural_S_ = plural_s ? "(S?)" : ""
  str = str.gsub(/\b#{word_1}#{plural_s_}\b/, "#{word_2}\\1")
  str = str.gsub(/\b#{word_1.capitalize}#{plural_s_}\b/, "#{word_2.capitalize}\\1")
  str = str.gsub(/\b#{word_1.upcase}#{plural_S_}\b/, "#{word_2.upcase}\\1")
  str
end

def xml_str_to_node_list(str)
  Nokogiri::XML("<line>" + str + "</line>").children.first.children
end

def node_list_to_text(node_list)
  node_list.collect { |node| node.text }.join
end


class Verse
  attr_reader :book, :book_number, :chapter_number, :section, :verse_number, :content, :word_count, :word_count_2

  def initialize(opts, data)
    @opts = opts
    @book, @book_number, @chapter_number, @section, @verse_number, @content, @word_count, @word_count_2 = nil
    parse(data)
    raise("FATAL: missing value") if @book.nil? || @chapter_number.nil? || @verse_number.nil? || @content.nil?
    @book_number = Books.index(@book)
    raise("FATAL: no such book: #{@book}") unless @book_number
    @section = process_text(@section, is_section: true) if @section
    @content = process_text(@content)
    if ! @content || @content.empty?
      msg = "empty verse: #{@book} #{@chapter_number}:#{@verse_number}"
      raise("FATAL: #{msg}") unless @opts.allow_empty_verses?
      puts("WARNING: #{msg}")
    end
    update_word_count
  end

  def parse(data)
  end

  def update_word_count
    @word_count = count_words(@content)
    @word_count_2 = count_words(@content.gsub(/\p{Pd}/, " "))
  end

  def process_text(str, is_section: false)
    str = str.gsub(/\p{Zs}+/, " ") # replace sequences of whitespace by a single space
    str = str.gsub(/\/ \//, " ") # no need to stop italics for a space
    str = str.gsub(/\/\//, "") # two consecutive slashs can be removed
    str = str.gsub(/^\p{Zs}+/, "").gsub(/\p{Zs}+$/, "") # remove whitespace at beginning and end of lines
    str = str.gsub(/\/([\p{Pd}\p{Po}\p{Zs}]+)\//, "\\1") if @opts.normalize_italics_1?
    str = str.gsub(/([:;,.?!])\//, "/\\1") if @opts.normalize_italics_2?
    str = str.gsub(/\A\/?\p{L}+\/?\b/, &:upcase).gsub(/\A\/?\p{L}\/? \/?\p{L}+\/?\b/, &:upcase) if @opts.upcase_chapter_start? && @verse_number == 1 && ! is_section
    if @opts.remove_dashes?
      dash_regex = /\b(\/?)(\p{Lu}\p{L}*)\p{Pd}(\p{Ll})/
      while dash_regex.match(str)
        str = str.gsub(dash_regex, "\\1\\2\\3")
      end
    end
    str = str.gsub(/\p{Pd}/, "-") if @opts.normalize_dashes?
    str = str.gsub(/[‘']/, "’") if ! @opts.no_normalize_apostrophes?
    if ! @opts.no_normalize_italics?
      str = str.gsub(/\/([‘'’])+\//, "\\1")
      str = str.gsub(/\[\//, "[ /").gsub(/\/\]/, "/ ]")
    end
    str = str.gsub(/([‘'’])S\b/, "\\1s") if @opts.downcase_apostrophe_s?
    str = str.gsub(/Æ/, "AE").gsub(/æ/, "ae") if @opts.transliterate_ae?
    if @opts.normalize_spelling?
      [["ankle", "ancle", true],
       ["counsellor", "counseller", true],
       ["enquire", "inquire", false],
       ["enquired", "inquired", false],
       ["enquirest", "inquirest", false],
       ["enquiry", "inquiry", false],
       ["expenses", "expences", false],
       ["razor", "rasor", false]].each do |x|
        str = replace_word(str, x[0], x[1], plural_s: x[2])
      end
    end
    str
  end

  def remove_section
    @section = nil
  end

  def <=>(other)
    [@book_number, @chapter_number, @verse_number] <=> [other.book_number, other.chapter_number, other.verse_number]
  end
end

class VerseImp < Verse
  def parse(data)
    @book = data[:book]
    @chapter_number = data[:chapter_number].to_i
    @verse_number = data[:verse_number].to_i
    @content = node_list_to_text(xml_str_to_node_list(data[:text]))
  end

  def node_list_to_text(node_list, params={})
    node_list.collect do |node|
      if node.name == "note"
        ""
      elsif node.name == "title"
        section = node_list_to_text(node.children)
        if @section
          @section += "\n\n" + section
        else
          @section = section
        end
        ""
      elsif ! @opts.no_pilcrows? && node.name == "milestone" && node["marker"] == "¶" && node["type"] == "x-p"
        "¶ "
      elsif ! node.children.empty?
        prefix = ""
        suffix = ""
        pa = params.clone
        if ! @opts.no_divine_names_upcase? && node.name == "divineName"
          pa["upcase"] = true
        elsif (! @opts.no_added_words_italics? && node.name == "transChange" && node["type"] == "added") || (node.name == "hi" && node["type"] == "italic")
          prefix = "/"
          suffix = "/"
        elsif node.name == "hi" && node["type"] == "bold"
          prefix = "*"
          suffix = "*"
        elsif node.name == "foreign" && node["n"]
          prefix = node["n"] + " "
        elsif node.name == "div" && node["type"] == "colophon"
          prefix = "\n\n"
        end
        prefix + node_list_to_text(node.children, pa) + suffix
      elsif (node.name == "closer" && node["sID"]) || (node.name == "div" && node["type"] == "colophon" && node["sID"])
        "\n\n"
      elsif node.name == "text"
        x = node.text
        x = x.upcase if params["upcase"]
        x
      else
        ""
      end
    end.join
  end
end

class VersePlain < Verse
  ## Format that uses a certain non-XML inline markup.
  def process_text(str, is_section: false)
    res = ""
    nesting = 0
    str.split("").each do |x|
      if x == "["
        nesting += 1
        if nesting == 1 && ! @opts.no_added_words_italics?
          res += "/"
        elsif nesting > 1
          res += "["
        end
      elsif x == "]"
        nesting -= 1
        if nesting == 0 && ! @opts.no_added_words_italics?
          res += "/"
        elsif nesting > 0
          res += "]"
        end
      else
        res += x
      end
    end
    super(res, is_section: is_section)
  end
end

class VerseAv1611Com < VersePlain
  def parse(data)
    meta = data[:meta].strip
    text = data[:text].strip
    m_meta = /\A\$\$ (\S+) (\d+):(\d+)\z/.match(meta)
    raise("FATAL: no match: »#{meta}«") unless m_meta && m_meta.size == 4
    section = nil
    if m = /\A<<(.*)>> (.*)\z/.match(text)
      section = m[1]
      text = m[2]
    end
    if m = /\A(.*) <<(.*)>>\z/.match(text)
      text = "#{m[1].strip}\n\n#{m[2]}"
    end
    @book, @chapter_number, @section, @verse_number, @content = Books.translate(m_meta[1], :av1611com, :sword), m_meta[2].to_i, section, m_meta[3].to_i, text
  end
end

class VerseBibleProtectorComText < VerseAv1611Com
  def parse(data)
    data.gsub!(/ <()<THE END.>>\z/, "")
    data.gsub!(/ <()<THE END OF THE PROPHETS.>>\z/, "")
    data.gsub!("", "'")
    m = /\A(\S+ \d+:\d+) (.*)\z/.match(data)
    raise("FATAL: no match: »#{data}«") unless m && m.length == 3
    super({ meta: "$$ #{m[1]}", text: m[2] })
  end
end

class VerseBibleProtectorComDoc < Verse
  def parse(data)
    @book = data[:book]
    @chapter_number = data[:chapter_number]
    if data[:section_node_list]
      @section = data[:section_node_list].collect { |node| node_to_text(node, "T13", "T15", nil) }.join
    end
    @verse_number = data[:verse_number]
    @content = data[:content_node_list].collect.with_index do |node, i|
      x = node_to_text(node, "T11", "T9", "T12")
      if i == 0 && @verse_number > 1
        m = /\A(\d+) +(.*)/.match(x)
        raise("FATAL: verse number mismatch: #{@book} #{@chapter_number}:#{@verse_number} »#{x}«") unless m && m.size == 3 && m[1].to_i == @verse_number
        x = m[2]
      end
      x
    end.join
  end

  def node_to_text(node, divine_names_class, added_words_class, both_class)
    x = node.text
    x = x.gsub(/¶\p{Zs}*/, "") if @opts.no_pilcrows?
    x = x.upcase if ! @opts.no_divine_names_upcase? && node["class"] == divine_names_class && divine_names_class
    x = "/" + x + "/" if ! @opts.no_added_words_italics? && node["class"] == added_words_class && added_words_class
    if node["class"] == both_class && both_class
      x = x.upcase if ! @opts.no_divine_names_upcase?
      x = "/" + x + "/" if ! @opts.no_added_words_italics?
    end
    x
  end

  def add_colophon(node_list)
    x = node_list.collect { |node| node_to_text(node, nil, "T15", nil) }.join
    @content = @content + "\n\n" + process_text(x)
    update_word_count
  end
end


class VerseList
  def initialize(opts)
    @opts = opts
    @stats_ref = STATS[@opts[:text_standard]]
    unless @stats_ref
      puts "WARNING: There are no reference statistics for this text standard, or no text standard specified;"
      puts "WARNING: so most statistics checks will be skipped."
    end
    @the_list = []
  end

  def run
    puts "Converting to internal format ..."
    build_list
    @the_list.sort!
    fix_list
    check_stats_1
    print_org
  end

  def build_list
  end

  def fix_list
    ## If more than one consecutive verses have the same section
    ## heading, this is assumed to be an error, and only the first of
    ## these verses will keep its section heading.
    chapter_number = nil
    section = nil
    @the_list.each do |verse|
      if ! chapter_number || chapter_number != verse.chapter_number
        chapter_number = verse.chapter_number
        section = verse.section
      else
        if verse.section && section == verse.section
          verse.remove_section
        else
          section = verse.section
        end
      end
    end
  end

  def check_stats_1
    puts "Statistics check 1 ..."
    if @opts[:counts_file]
      counts_file = File.open(@opts[:counts_file], "w")
      counts_file.write("Book,ChapterNumber,WordCount\n")
    end
    word_count_in_book = 0
    word_count_in_chapter = 0
    word_count_2_in_book = 0
    word_count_sections_in_book = 0
    i = 0
    while i < @the_list.size
      verse = @the_list[i]
      word_count_in_book += verse.word_count
      word_count_in_chapter += verse.word_count
      word_count_2_in_book += verse.word_count_2
      word_count_sections_in_book += count_words(verse.section)
      i += 1
      if (i == @the_list.size || verse.chapter_number != @the_list[i].chapter_number) && counts_file
        counts_file.write("#{verse.book},#{verse.chapter_number},#{word_count_in_chapter}\n")
        word_count_in_chapter = 0
      end
      if i == @the_list.size || verse.book != @the_list[i].book
        raise("FATAL: word count mismatch in #{verse.book}: #{word_count_in_book} (#{@stats_ref[verse.book][2]})") if @stats_ref && word_count_in_book != @stats_ref[verse.book][2]
        puts "Different word count without and with split at dash in #{verse.book}: #{word_count_in_book}, #{word_count_2_in_book}" if word_count_in_book != word_count_2_in_book
        puts "Word count in section headings in #{verse.book}: #{word_count_sections_in_book}" if word_count_sections_in_book > 0
        word_count_in_book = 0
        word_count_2_in_book = 0
        word_count_sections_in_book = 0
      end
    end
    counts_file.close if counts_file
  end

  def check_stats_2(book, chapter_count_in_book, verse_count_in_book, word_count_in_book)
    if book && @stats_ref
      chapter_count_in_book_ref, verse_count_in_book_ref, word_count_in_book_ref = @stats_ref[book]
      if chapter_count_in_book != chapter_count_in_book_ref || verse_count_in_book != verse_count_in_book_ref || word_count_in_book != word_count_in_book_ref
        s = "FATAL: statistics mismatch in #{book}: "
        s += "#{chapter_count_in_book} (#{chapter_count_in_book_ref}), "
        s += "#{verse_count_in_book} (#{verse_count_in_book_ref}), "
        s += "#{word_count_in_book} (#{word_count_in_book_ref})"
        raise(s)
      end
      puts "Statistics check 2 OK: #{book}: #{chapter_count_in_book}, #{verse_count_in_book}, #{word_count_in_book}"
    end
  end

  def print_org
    puts "Printing Org Mode file ..."
    book = nil
    book_count = 0
    chapter_number = nil
    chapter_count_in_book = 0
    chapter_count = 0
    verse_number = nil
    verse_count_in_book = 0
    verse_count = 0
    word_count_in_book = 0
    word_count = 0
    File.open(@opts[:outfile], "w") do |file|
      file.write("#+STARTUP: entitiespretty\n")
      @the_list.each do |verse|
        output = ""
        new_book = false
        new_chapter = false
        new_section = false
        if book != verse.book
          check_stats_2(book, chapter_count_in_book, verse_count_in_book, word_count_in_book)
          book = verse.book
          chapter_number = nil
          new_book = true
          book_count += 1
          chapter_count_in_book = 0
          verse_count_in_book = 0
          word_count_in_book = 0
          output += "\n* #{book}\n"
        end
        if chapter_number != verse.chapter_number
          unless (chapter_number.nil? && verse.chapter_number == 1) || (chapter_number && chapter_number + 1 == verse.chapter_number)
            raise("FATAL: non-consecutive chapter numbers: #{chapter_number} in #{book}")
          end
          chapter_number = verse.chapter_number
          verse_number = nil
          new_chapter = true
          chapter_count_in_book += 1
          chapter_count += 1
          output += "\n** Chapter #{chapter_number}\n"
        end
        if verse.section
          new_section = true
          output += "\n#{verse.section}\n"
        end
        unless (verse_number.nil? && verse.verse_number == 1) || (verse_number && verse_number + 1 == verse.verse_number)
          raise("FATAL: non-consecutive verse numbers: #{verse_number} in #{chapter_number} in #{book}")
        end
        verse_number = verse.verse_number
        raise("FATAL: new book but no new chapter: #{verse_number} in #{chapter_number} in #{book}") if new_book && ! new_chapter
        verse_count_in_book += 1
        verse_count += 1
        word_count_in_book += verse.word_count
        word_count += verse.word_count
        output += "\n" if new_book || new_chapter || new_section
        output += "﻿^{#{verse_number}} #{verse.content}\n" # there is an invisible space at the beginning of the string
        file.write(output)
      end
    end
    check_stats_2(book, chapter_count_in_book, verse_count_in_book, word_count_in_book)
    puts "Total number of given verses: #{@the_list.length}"
    puts "Total number of printed books, chapters, verses, words: #{book_count}, #{chapter_count}, #{verse_count}, #{word_count}"
    puts "Org Mode file created successfully."
  end
end

class VerseListFromFile < VerseList
  VERSE_CLASS = nil
  def initialize(opts)
    super(opts)
    puts "Reading input file ..."
    infile = @opts[:infile] ? File.open(@opts[:infile], :encoding => @opts[:encoding]) : $stdin
    @line_list = infile.readlines.collect { |line| line.encode("UTF-8").chomp }
    infile.close
  end
  def build_list
    @the_list = @line_list.map { |line| self.class::VERSE_CLASS.new(@opts, line) }
  end
end

class VerseListImp < VerseListFromFile
  def build_list
    i = 0
    check_book = nil
    check_book_osis = nil
    check_chapter_number = nil
    skipped_books = {}
    while i < @line_list.size
      meta = @line_list[i]
      text = @line_list[i+1]
      if meta.empty?
        i += 1
        next
      end
      m1_meta = /\A\$\$\$(.*)\z/.match(meta)
      raise("FATAL: no match: »#{meta}«") unless m1_meta && m1_meta.size == 2
      if m2_meta = /\A(\p{Lu}\D+) (\d+):(\d+)\z/.match(m1_meta[1])
        raise("FATAL: no match: »#{m1_meta[1]}«") unless m2_meta && m2_meta.size == 4
        book = m2_meta[1]
        chapter_number = m2_meta[2]
        verse_number = m2_meta[3]
        if chapter_number == "0" && verse_number == "0" && ! @opts.relax?
          node_list = xml_str_to_node_list(text)
          raise("FATAL: malformed XML for book: »#{text}«") if node_list.empty? || node_list.first.name != "div" || node_list.first["osisID"].nil? || node_list.first["type"] != "book"
          check_book_osis = node_list.first["osisID"]
          check_book = Books.translate(check_book_osis, :osis, :sword)
          raise("FATAL: book mismatch: #{book} (#{check_book})") if book != check_book
        elsif verse_number == "0" && ! @opts.relax?
          node_list = xml_str_to_node_list(text)
          raise("FATAL: malformed XML for chapter: »#{text}«") if node_list.empty? || node_list.first.name != "chapter" || node_list.first["osisID"].nil?
          m = /\A#{check_book_osis}\.(\d+)\z/.match(node_list.first["osisID"])
          raise("FATAL: no match: »#{node_list.first["osisID"]}«") unless m && m.size == 2
          check_chapter_number = m[1]
          raise("FATAL: chapter number mismatch: #{chapter_number} (#{check_chapter_number})") if chapter_number != check_chapter_number
        elsif chapter_number == "1" && verse_number == "1" && text.empty? && @opts.skip_empty_books?
          skipped_books[book] = true
        elsif ! text.empty? && @opts.skip_empty_books? && skipped_books[book]
          raise("FATAL: non-empty verse in a skipped book: #{book} #{chapter_number}:#{verse_number}")
        elsif verse_number != "0" && ! (@opts.skip_empty_books? && skipped_books[book])
          raise("FATAL: book mismatch: #{book} (#{check_book})") if ! @opts.relax? && book != check_book
          raise("FATAL: chapter number mismatch: #{chapter_number} (#{check_chapter_number})") if ! @opts.relax? && chapter_number != check_chapter_number
          @the_list << VerseImp.new(@opts, { book: book, chapter_number: chapter_number, verse_number: verse_number, text: text })
        end
      elsif /\A\[ .* \]\z/.match(m1_meta[1]).nil?
        raise("FATAL: no match: »#{m1_meta[1]}«")
      end
      i += 2
    end
  end
end

class VerseListAv1611Com < VerseListFromFile
  def build_list
    i = 0
    while i < @line_list.size
      @the_list << VerseAv1611Com.new(@opts, { meta: @line_list[i], text: @line_list[i+1] })
      i += 3
    end
  end
end

class VerseListBibleProtectorComText < VerseListFromFile
  VERSE_CLASS = VerseBibleProtectorComText
end

class VerseListBibleProtectorComDoc < VerseListFromFile
  def build_list
    doc = Nokogiri::XML(@line_list.join("\n"))
    div_section_index = 5
    book_index = 0
    section_node_list = nil
    while book_index < 66
      book = Books.name(book_index)
      chapter_number = 0
      verse_number = nil
      section_node_list = nil
      doc.xpath("//html:div[@id='Section#{div_section_index}']", { "html" => "http://www.w3.org/1999/xhtml" } ).children.each do |node|
        node_children = node.children.reject { |x| /\A\n +\z/.match(x.text) }
        if node.name == "p" && ["P9", "P10", "P22"].include?(node["class"]) && m = /\A(CHAPTER|PSALM) (\d+)/.match(node.text.strip)
          x = m[2].to_i
          raise("FATAL: non-consecutive chapter numbers at: #{book} #{chapter_number}:#{verse_number}") if x != chapter_number + 1
          chapter_number = x
          verse_number = 0
        elsif node.name == "p" && ["P10", "P22", "P26", "Text_20_body"].include?(node["class"])
          section_node_list = node_children
        elsif node.name == "p" && ["P23", "P25"].include?(node["class"]) && ! node_children.empty? && ! /\A\p{Zs}*\z/.match(node_list_to_text(node_children))
          verse_number += 1
          @the_list << VerseBibleProtectorComDoc.new(@opts, { book: book, chapter_number: chapter_number, section_node_list: section_node_list, verse_number: verse_number, content_node_list: node_children })
          section_node_list = nil
        elsif node.name == "p" && ["P28"].include?(node["class"])
          @the_list.last.add_colophon(node_children)
        end
      end
      book_index += 1 if chapter_number > 0
      div_section_index += 1
    end
  end
end
